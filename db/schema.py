import sqlite3
import os

from sql import *


db_loc = os.path.join(os.path.dirname(__file__), 'database.sqlite')


def connector():
    conn = sqlite3.connect(db_loc)
    conn.text_factory = str
    return conn

init(connector, sqlite3.Row)


class User(BaseMapper):
    tablename = 'user'
    columns = (VarChar('name', 20), VarChar('pswd', 64), Integer('gm'), 
               Integer('gender'), Integer('level'), Integer('xp'), 
               Integer('gold'), Integer('map'), Integer('x'), Integer('y'), 
               Integer('str'), Integer('vit'), Integer('agi'), Integer('dex'), 
               Integer('ski'))


class Item(BaseMapper):
    tablename = 'item'
    columns = (Integer('user_id'), Integer('type'), Integer('quantity'), 
               Integer('slot'), Integer('loc'), Integer('equipped'))


initialize_database([User, Item])
