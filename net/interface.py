import json
import struct


PORT = 27750


class ConnectionError(Exception):
    """
    Error raised when connection termination or corruption is detected.
    """


class Connection(object):
    def __init__(self, sock, chunk=2048):
        sock.settimeout(10)
        self.sock = sock
        self.chunk = chunk
        self.buff = ''
    
    def recv(self):
        """
        Get and decode the next packet.
        """
        # Get the packet size (up to 65536 bytes)
        psize = struct.unpack('>H', self.sock.recv(2))[0]
        
        while len(self.buff) < psize:
            data = self.sock.recv(self.chunk)
            if not data:
                raise ConnectionError('Promised data not available!')
            self.buff += data
        
        packet = self.buff[:psize]
        # Save any left-overs!
        self.buff = self.buff[psize:]
        
        # Decode and return
        return json.loads(packet)
    
    def send(self, obj):
        """
        Encode and send a packet.
        """
        data = json.dumps(obj)
        
        size = len(data)
        if size > 65536:
            raise ValueError('This object is too large to send!')
        
        packet = struct.pack('>H', size) + data
        self.sock.send(packet)
