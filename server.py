from thread import allocate_lock, start_new_thread
from hashlib import sha256
import socket
import random
import string

from net.interface import *
from net.messages import *
from db.schema import *


allowed_chars = string.ascii_letters + string.digits


def threaded(func, *args, **kw):
    start_new_thread(func, args, kw)


def identifier():
    return sha256(str(random.random())).hexdigest()


def check_name(name):
    for c in name:
        if c not in allowed_chars:
            return False
    return True


class Clients(object):
    def __init__(self, engine, ping_cycle=5):
        engine.add_cb(C_PONG, self.got_pong)
        self.engine = engine
        
        self.clients = {}
        self.lock = allocate_lock()
        self.active_pings = {}
        self.plock = allocate_lock()
        self.ping_cycle = ping_cycle
    
    def add(self, conn):
        with self.lock:
            self.clients[conn.row.id] = conn
    
    def remove(self, id):
        self.clients.pop(id).close()
    
    def got_pong(self, cid, id, args):
        passed = args[0]
        with self.plock, self.lock:
            stored = self.active_pings.pop(cid, None)
            # Sorry, we use "smart" pings; this catches bad responses and 
            # unrequested responses
            if passed != stored:
                self.remove(cid)
    
    def ping_loop(self):
        while True:
            time.sleep(self.ping_cycle)
            
            # We give clients a certain amount of time to respond to the ping.
            # After that time, all unponged pings are considered dead and their 
            # clients removed.
            with self.plock, self.lock:
                for id in self.active_pings:
                    self.remove(id)
                
                self.active_pings = {}
                
                for id, c in self.clients.items():
                    self.pings[id] = self.engine.ping(c)


class Engine(object):
    def __init__(self):
        self.cbs = {}
        self.lock = allocate_lock()
    
    def add_cb(self, id, cb):
        with self.lock:
            self.cbs.setdefault(id, []).append(cb)
    
    def callback(self, cid, msg):
        id = msg[0]
        for cb in self.cbs.get(id, ()):
            cb(cid, id, msg[1:])
    
    def ping(self, c):
        id = identifier()
        c.send([S_PING, id])
        return id


class Server(object):
    def __init__(self, host='0.0.0.0', port=PORT):
        self.clients = Clients()
        self.host = (host, port)
    
    def main(self):
        try:
            ## Get action going
            threaded(self._action, conn)
            
            ## Get socket going
            self.sock = socket.socket()
            self.sock.bind(self.host)
            self.sock.listen(5)
            
            while True:
                conn, addr = self.sock.accept()
                threaded(self._handler, conn)
        
        finally:
            self.sock.close()
    
    def _handler(self, conn):
        ok = False
        conn = Connection(conn)
        
        msg = conn.recv()
        id = msg[0]
        
        if id == C_REGISTER:
            name, pswd, gender = msg[1:]
            if not check_name(name):
                conn.send([S_AUTH_RES, False, 
                           'Username contains illegal characters!'])
            elif User.get(name=name):
                conn.send([S_AUTH_RES, False, 'User already exists!'])
            else:
                user = User(dict(name=name, pswd=pswd, gender=gender))
                user.add()
                get_connection().commit()
                ok = True
            
            
        elif id == C_LOGIN:
            name, pswd = msg[1:]
            user = User.get(name=name, pswd=pswd)
            if not user:
                conn.send([S_AUTH_RES, False])
        
        if not ok:
            conn.close()
            return
        
        conn.send([S_AUTH_RES, True, 'Logged in successfully.'])
        self.clients.add(conn)
